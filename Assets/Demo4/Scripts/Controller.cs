using UnityEngine;
using UnityEngine.Rendering;
using UnityEngine.UI;

namespace Demo4
{
    public class Controller : MonoBehaviour
    {
        [SerializeField]
        private RawImage _image = null;

        [SerializeField]
        private Brush _brush = null;

        [SerializeField]
        private ProgressCalculator _progress = null;

        private RenderTexture _renderTexture = null;
        private CommandBuffer _cmd = null;



        private void Awake()
        {
            _renderTexture = new RenderTexture(1024, 1024, 0, RenderTextureFormat.R8);
            _image.texture = _renderTexture;

            _brush.SetTarget(_image.GetComponent<RectTransform>(), _renderTexture);

            Restart();
        }


        private void OnDestroy()
        {
            _renderTexture.Release();
            _renderTexture = null;

            _cmd?.Dispose();
            _cmd = null;
        }



        public void Restart()
        {
            if (_cmd == null)
            {
                _cmd = new CommandBuffer();
                _cmd.SetRenderTarget(_renderTexture);
                _cmd.ClearRenderTarget(false, true, Color.white);
            }

            Graphics.ExecuteCommandBuffer(_cmd);

            _progress.SetTexture(_renderTexture);
        }
    }
}
