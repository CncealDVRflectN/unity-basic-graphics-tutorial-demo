﻿using UnityEngine;
using UnityEngine.Events;
using UnityEngine.EventSystems;
using UnityEngine.Rendering;

namespace Demo4
{
    public class Brush : MonoBehaviour, IBeginDragHandler, IDragHandler
    {
        private static readonly int s_brushLineProperty = Shader.PropertyToID("_BrushLine");
        private static readonly int s_brushRadiusProperty = Shader.PropertyToID("_BrushRadius");
        private static readonly int s_rectSizeProperty = Shader.PropertyToID("_RectSize");

        public UnityEvent OnApplied;

        [SerializeField, Min(0f)]
        private float _radius = 50f;

        private RenderTexture _targetRT = null;
        private RectTransform _targetTransform = null;
        private CommandBuffer _cmd = null;
        private Material _material = null;

        private Vector2 _prevPos = Vector2.negativeInfinity;
        private Vector2 _currentPos = Vector2.negativeInfinity;



        private void Awake()
        {
            _cmd = new CommandBuffer();
            _material = new Material(Shader.Find("Hidden/Demo4/Brush"));
        }


        private void OnDestroy()
        {
            _cmd.Dispose();
            _cmd = null;

            Destroy(_material);
            _material = null;
        }



        public void OnBeginDrag(PointerEventData eventData) => SetPosition(eventData.position);


        public void OnDrag(PointerEventData eventData) => Move(eventData.position);



        public void SetTarget(RectTransform targetTransform, RenderTexture target)
        {
            _targetTransform = targetTransform;
            _targetRT = target;
        }



        private void SetPosition(Vector2 screenPos)
        {
            _prevPos = Vector2.negativeInfinity;
            ScreenPositionToLocal(screenPos, out _currentPos);
        }


        private void Move(Vector2 screenPos)
        {
            if (ScreenPositionToLocal(screenPos, out Vector2 localPos))
            {
                _prevPos = _currentPos;
                _currentPos = localPos;

                Apply();
            }
        }


        private bool ScreenPositionToLocal(Vector2 screenPos, out Vector2 localPos)
        {
            return RectTransformUtility.ScreenPointToLocalPointInRectangle(
                _targetTransform, screenPos, null, out localPos);
        }



        private void Apply()
        {
            if (_targetRT == null) return;

            RenderTexture intermediateRT = RenderTexture.GetTemporary(
                _targetRT.width, _targetRT.height, _targetRT.depth, _targetRT.format);

            _material.SetVector(s_rectSizeProperty, _targetTransform.rect.size);
            _material.SetFloat(s_brushRadiusProperty, _radius);
            _material.SetVector(
                s_brushLineProperty, 
                new Vector4(_prevPos.x, _prevPos.y, _currentPos.x, _currentPos.y));

            _cmd.Clear();
            _cmd.SetRenderTarget(intermediateRT);
            _cmd.ClearRenderTarget(false, true, Color.clear);
            _cmd.Blit(_targetRT, BuiltinRenderTextureType.CurrentActive, _material);
            _cmd.SetRenderTarget(_targetRT);
            _cmd.Blit(intermediateRT, BuiltinRenderTextureType.CurrentActive);
            Graphics.ExecuteCommandBuffer(_cmd);

            RenderTexture.ReleaseTemporary(intermediateRT);

            OnApplied?.Invoke();
        }
    }
}
