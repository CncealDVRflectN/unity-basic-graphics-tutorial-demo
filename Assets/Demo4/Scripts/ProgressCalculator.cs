﻿using System.Collections.Generic;
using Unity.Collections;
using UnityEngine;
using UnityEngine.Rendering;
using UnityEngine.UI;

namespace Demo4
{
    // Downsampling chain optimization applied to unload CPU
    public class ProgressCalculator : MonoBehaviour
    {
        private const int DesiredTextureSize = 4;

        [SerializeField]
        private Text _label = null;

        private List<int> _chain = new List<int>();
        private RenderTexture _targetRT = null;
        private Texture2D _resultTexture = null;
        private CommandBuffer _cmd = null;
        private long _initialSum = 0;



        private void OnDestroy()
        {
            _cmd?.Dispose();
            _cmd = null;
        }



        public void SetTexture(RenderTexture renderTexture)
        {
            if (_cmd == null)
                _cmd = new CommandBuffer();

            _targetRT = renderTexture;
            CalculateChain();

            int resultSize = _chain[_chain.Count - 1];
            _resultTexture = new Texture2D(resultSize, resultSize, TextureFormat.R8, false);

            _initialSum = CalculateSum();
            UpdateProgress();
        }


        public void UpdateProgress()
        {
            long sum = CalculateSum();
            float progress = (float)sum / _initialSum;
            _label.text = $"{Mathf.RoundToInt(100f * progress)}%";
        }



        private void CalculateChain()
        {
            _chain.Clear();

            if (_targetRT.width <= DesiredTextureSize && _targetRT.height <= DesiredTextureSize)
                return;

            int size = Mathf.Max((_targetRT.width + 1) / 2, (_targetRT.height + 1) / 2);
            _chain.Add(size);

            while (size > DesiredTextureSize)
            {
                size = (size + 1) / 2;
                _chain.Add(size);
            }
        }



        private long CalculateSum()
        {
            var prevRT = _targetRT;

            for (int i = 0; i < _chain.Count; i++)
            {
                var nextRT = RenderTexture.GetTemporary(_chain[i], _chain[i], 0, prevRT.format);

                _cmd.Clear();
                _cmd.SetRenderTarget(nextRT);
                _cmd.ClearRenderTarget(false, true, Color.clear);
                _cmd.Blit(prevRT, BuiltinRenderTextureType.CurrentActive);
                Graphics.ExecuteCommandBuffer(_cmd);

                if (i > 0)
                    RenderTexture.ReleaseTemporary(prevRT);

                prevRT = nextRT;
            }

            RenderTexture.active = prevRT;
            _resultTexture.ReadPixels(new Rect(0f, 0f, _resultTexture.width, _resultTexture.height), 0, 0);

            NativeArray<byte> data = _resultTexture.GetRawTextureData<byte>();
            long sum = 0;
            for (int i = 0; i < data.Length; i++)
                sum += data[i];

            if (_chain.Count > 0)
            {
                RenderTexture.active = null;
                RenderTexture.ReleaseTemporary(prevRT);
            }

            return sum;
        }
    }
}
