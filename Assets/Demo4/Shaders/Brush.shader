Shader "Hidden/Demo4/Brush"
{
    Properties
    {
        [MainTexture] _MainTex("Texture", 2D) = "white" {}
    }
    SubShader
    {
        Cull Off 
        ZWrite Off 
        ZTest Off

        Pass
        {
            HLSLPROGRAM

            #pragma vertex vert
            #pragma fragment frag

            #include "Packages/com.unity.render-pipelines.universal/ShaderLibrary/Core.hlsl"

            struct Attributes
            {
                float4 positionOS : POSITION;
                float2 uv : TEXCOORD0;
            };

            struct Varyings
            {
                float4 positionCS : SV_POSITION;
                float2 uv : TEXCOORD0;
                float2 positionLS : TEXCOORD1;
            };


            sampler2D _MainTex;
            float4 _BrushLine;
            float2 _RectSize;
            float _BrushRadius;


            float CalculateBrushDistance(float2 localPosition)
            {
                float2 a = _BrushLine.xy;
                float2 b = _BrushLine.zw;
                float2 ab = b - a;
                float2 aToPoint = localPosition - a;
                float t = saturate(dot(aToPoint, ab) / dot(ab, ab));
                return length(aToPoint - ab * t);
            }


            Varyings vert(Attributes attribs)
            {
                Varyings vars;
                vars.positionCS = TransformObjectToHClip(attribs.positionOS.xyz);
                vars.uv = attribs.uv;
                vars.positionLS = (vars.uv - 0.5) * _RectSize;
                return vars;
            }


            half4 frag(Varyings vars) : SV_Target
            {
                half value = tex2D(_MainTex, vars.uv).r;
                float dist = CalculateBrushDistance(vars.positionLS);
                float distDelta = fwidth(dist);
                float intensity = 1.0 - smoothstep(
                    _BrushRadius - 0.5 * distDelta, _BrushRadius + 0.5 * distDelta, dist);

                value = max(0.0, value - intensity);
                return half4(value, 0.0, 0.0, 1.0);
            }

            ENDHLSL
        }
    }
}
