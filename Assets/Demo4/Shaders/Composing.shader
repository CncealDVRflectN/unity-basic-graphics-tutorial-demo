Shader "Demo4/Composing"
{
    Properties
    {
        [MainTexture] _MainTex("Mask", 2D) = "white" {}
    }
    SubShader
    {
        Tags
        {
            "Queue"="Transparent"
        }
        
        Cull Off 
        ZWrite Off 
        ZTest On
        Blend SrcAlpha OneMinusSrcAlpha

        Pass
        {
            HLSLPROGRAM

            #pragma vertex vert
            #pragma fragment frag

            #include "Packages/com.unity.render-pipelines.universal/ShaderLibrary/Core.hlsl"

            struct Attributes
            {
                float4 positionOS : POSITION;
                float2 uv : TEXCOORD0;
                half4 color : COLOR;
            };

            struct Varyings
            {
                float4 positionCS : SV_POSITION;
                float2 uv : TEXCOORD0;
                half4 color : COLOR;
            };


            sampler2D _MainTex;


            Varyings vert(Attributes attribs)
            {
                Varyings vars;
                vars.positionCS = TransformObjectToHClip(attribs.positionOS.xyz);
                vars.uv = attribs.uv;
                vars.color = attribs.color;
                return vars;
            }


            half4 frag(Varyings vars) : SV_Target
            {
                float maskValue = tex2D(_MainTex, vars.uv).r;

                if (maskValue < 0.01)
                    discard;

                return half4(vars.color.rgb, vars.color.a * maskValue);
            }

            ENDHLSL
        }
    }
}
