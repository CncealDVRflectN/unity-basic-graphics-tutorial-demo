Shader "Demo3/GrayscaleStencilFill"
{
    HLSLINCLUDE

    #include "Packages/com.unity.render-pipelines.universal/ShaderLibrary/Core.hlsl"

    struct Attributes
    {
        float3 positionOS : POSITION;
    };

    struct Varyings
    {
        float4 positionCS : SV_POSITION;
    };

    Varyings vert(Attributes attribs)
    {
        Varyings vars;
        vars.positionCS = TransformObjectToHClip(attribs.positionOS);
        return vars;
    }

    half4 frag(Varyings vars) : SV_Target
    {
        return 1.0;
    }

    ENDHLSL


    SubShader
    {
        Tags { "Queue"="Transparent" }

        ColorMask 0
        ZWrite Off

        Pass
        {
            Name "StencilFill"

            ZTest Less
            Cull Off
            Stencil
            {
                Comp Always
                PassFront IncrSat
                ZFailBack IncrSat
            }

            HLSLPROGRAM

            #pragma vertex vert
            #pragma fragment frag

            ENDHLSL
        }

        Pass
        {
            Name "StencilCorrection"

            ZTest Less
            Cull Off
            Stencil
            {
                Comp Always
                PassBack DecrSat
                ZFailFront DecrSat
            }

            HLSLPROGRAM

            #pragma vertex vert
            #pragma fragment frag

            ENDHLSL
        }
    }
}
