Shader "Demo3/SphereGrayscale"
{
    Properties
    {
        _Position("Sphere Position (World Space)", Vector) = (0, 0, 0)
        _Radius("Sphere Radius (World Space)", Float) = 0.0
    }
    SubShader
    {
        ZTest Always
        ZWrite Off
        Cull Off

        Pass
        {
            HLSLPROGRAM

            #include "Packages/com.unity.render-pipelines.universal/ShaderLibrary/Core.hlsl"
            #include "Packages/com.unity.render-pipelines.universal/ShaderLibrary/DeclareDepthTexture.hlsl"
            #include "Packages/com.unity.render-pipelines.core/Runtime/Utilities/Blit.hlsl"
            
            #pragma vertex Vert
            #pragma fragment frag


            float3 _Position;
            float _Radius;


            half4 frag(Varyings vars) : SV_Target
            {
                half4 color = SAMPLE_TEXTURE2D_X(_BlitTexture, sampler_LinearClamp, vars.texcoord);

                #if UNITY_REVERSED_Z
                    float depth = SampleSceneDepth(vars.texcoord);
                    if (depth <= 0.0)
                        return half4(color.rgb, 1.0);
                #else
                    float depth = lerp(UNITY_NEAR_CLIP_VALUE, 1, SampleSceneDepth(uv));
                    if (depth >= 1.0)
                        return half4(color.rgb, 1.0);
                #endif

                float3 positionWS = ComputeWorldSpacePosition(vars.texcoord, depth, UNITY_MATRIX_I_VP);
                float dist = distance(positionWS, _Position);
                if (dist >= _Radius)
                    return half4(color.rgb, 1.0);

                half grayscale = dot(color.rgb, half3(0.2126, 0.7152, 0.0722));
                return half4(grayscale, grayscale, grayscale, 1.0);
            }

            ENDHLSL
        }
    }
}
