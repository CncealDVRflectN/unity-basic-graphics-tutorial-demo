Shader "Demo3/GrayscaleStencil"
{
    SubShader
    {
        ZTest Always
        ZWrite Off
        Cull Off
        Stencil
        {
            Ref 0
            Comp Less
        }

        Pass
        {
            HLSLPROGRAM

            #include "Packages/com.unity.render-pipelines.universal/ShaderLibrary/Core.hlsl"
            #include "Packages/com.unity.render-pipelines.core/Runtime/Utilities/Blit.hlsl"

            #pragma vertex Vert
            #pragma fragment frag


            half4 frag(Varyings vars) : SV_Target
            {
                half4 color = SAMPLE_TEXTURE2D_X(_BlitTexture, sampler_LinearClamp, vars.texcoord);
                half grayscale = dot(color.rgb, half3(0.2126, 0.7152, 0.0722));
                return half4(grayscale, grayscale, grayscale, 1.0);
            }

            ENDHLSL
        }
    }
}
