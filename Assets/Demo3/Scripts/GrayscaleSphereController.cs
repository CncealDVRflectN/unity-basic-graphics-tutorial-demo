using UnityEngine;
using UnityEngine.Rendering.Universal;

namespace Demo3
{
    [ExecuteInEditMode]
    public class GrayscaleSphereController : MonoBehaviour
    {
        private static readonly int PositionPropertyID = Shader.PropertyToID("_Position");
        private static readonly int RadiusPropertyID = Shader.PropertyToID("_Radius");

        [SerializeField]
        private ScriptableRendererData _rendererData = null;

        [SerializeField, Min(0f)]
        private float _radius = 1f;



        private void Update()
        {
            if (!_rendererData) return;

            var features = _rendererData.rendererFeatures;
            for (int i = 0; i < features.Count; i++)
            {
                if (features[i] is FullScreenPassRendererFeature fullScreenPass &&
                    fullScreenPass.passMaterial.shader.name.Equals("Demo3/SphereGrayscale"))
                {
                    fullScreenPass.passMaterial.SetVector(PositionPropertyID, transform.position);
                    fullScreenPass.passMaterial.SetFloat(RadiusPropertyID, _radius);
                }
            }
        }
    }
}
