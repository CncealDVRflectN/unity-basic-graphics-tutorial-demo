﻿using System;
using UnityEngine;
using UnityEngine.Rendering;
using UnityEngine.Rendering.Universal;


namespace Demo3
{
    public class GrayscaleStencilPassFeature : ScriptableRendererFeature
    {
        private GrayscaleStencilPass _pass = null;


        public override void Create() => _pass = new GrayscaleStencilPass();


        public override void AddRenderPasses(ScriptableRenderer renderer, ref RenderingData renderingData)
        {
            if (IsCameraSupported(ref renderingData.cameraData))
            {
                _pass.SetTarget(renderingData.cameraData);
                renderer.EnqueuePass(_pass);
            }
        }



        protected override void Dispose(bool disposing) => _pass.Dispose();



        private bool IsCameraSupported(ref CameraData cameraData)
        {
            return cameraData.cameraType == CameraType.Game || 
                cameraData.cameraType == CameraType.SceneView;
        }



        private class GrayscaleStencilPass : ScriptableRenderPass, IDisposable
        {
            private static readonly int BlitTexturePropertyID = Shader.PropertyToID("_BlitTexture");

            private readonly ProfilingSampler _profilingSampler = new ProfilingSampler("GrayscaleStencil");
            private readonly Material _material = null;
            private RTHandle _colorRT = null;



            public GrayscaleStencilPass()
            {
                _material = CoreUtils.CreateEngineMaterial(Shader.Find("Demo3/GrayscaleStencil"));
                renderPassEvent = RenderPassEvent.BeforeRenderingTransparents;
            }


            public void Dispose()
            {
                CoreUtils.Destroy(_material);
                _colorRT?.Release();
                _colorRT = null;
            }



            public void SetTarget(CameraData cameraData)
            {
                var descriptor = cameraData.cameraTargetDescriptor;
                descriptor.depthBufferBits = (int)DepthBits.None;
                RenderingUtils.ReAllocateIfNeeded(ref _colorRT, descriptor);
            }


            public override void Execute(ScriptableRenderContext context, ref RenderingData renderingData)
            {
                if (!_material || _colorRT == null)
                    return;

                var cameraColor = renderingData.cameraData.renderer.cameraColorTargetHandle;
                var cameraDepth = renderingData.cameraData.renderer.cameraDepthTargetHandle;

                var cmd = CommandBufferPool.Get();
                using (new ProfilingScope(cmd, _profilingSampler))
                {
                    Blitter.BlitCameraTexture(cmd, cameraColor, _colorRT);
                    _material.SetTexture(BlitTexturePropertyID, _colorRT);

                    CoreUtils.SetRenderTarget(cmd, cameraColor, cameraDepth);
                    CoreUtils.DrawFullScreen(cmd, _material, null, 0);
                    context.ExecuteCommandBuffer(cmd);
                }

                cmd.Clear();
                CommandBufferPool.Release(cmd);
            }
        }
    }
}
