Shader "Demo2/Water"
{
    Properties
    {
        _Color("Main Color", Color) = (0, 0, 0.65, 1)
        _SpecularStrength("Specular Strength", Float) = 0.5
        _Shininess("Shininess", Float) = 32.0

        _WavesDirAngle("Waves Direction Angle", Range(0.0, 360.0)) = 0.0
        _WavesAmp("Waves Amplitude", Float) = 0.5
        _WavesSpeed("Waves Speed", Float) = 1.0
        _WavesFreq("Waves Frequency", Float) = 1.0

        _SecondaryWavesDirAngle("Secondary Waves Direction Angle", Range(0.0, 360.0)) = 0.0
        _SecondaryWavesAmp("Secondary Waves Amplitude", Float) = 0.1
        _SecondaryWavesSpeed("Secondary Waves Speed", Float) = 0.5
        _SecondaryWavesFreq("Secondary Waves Frequency", Float) = 3.0
    }
    SubShader
    {
        Tags { "Queue"="Transparent" }

        Blend SrcAlpha OneMinusSrcAlpha

        Pass
        {
            HLSLPROGRAM

            #pragma vertex vert
            #pragma fragment frag

            #include "Packages/com.unity.render-pipelines.universal/ShaderLibrary/Core.hlsl"

            struct Attributes
            {
                float3 positionOS : POSITION;
                float3 normalOS : NORMAL;
            };

            struct Varyings
            {
                float4 positionCS : SV_POSITION;
                float3 positionWS : TEXCOORD0;
            };


            float4 _Color;
            float _SpecularStrength;
            float _Shininess;
            float _WavesDirAngle;
            float _WavesAmp;
            float _WavesSpeed;
            float _WavesFreq;
            float _SecondaryWavesDirAngle;
            float _SecondaryWavesAmp;
            float _SecondaryWavesSpeed;
            float _SecondaryWavesFreq;


            float CalculateWave(float3 positionWS, float dirAngle, float amplitude, float frequency, float speed)
            {
                float dirRadians = (dirAngle / 180.0) * PI;
                float3 dirWS = float3(sin(dirRadians), 0.0, cos(dirRadians));
                float param = frequency * (dot(positionWS, dirWS) + speed * _Time.y);
                return amplitude * sin(param);
            }


            Varyings vert(Attributes attribs)
            {
                Varyings vars;
                vars.positionWS = TransformObjectToWorld(attribs.positionOS);

                float mainWave = CalculateWave(vars.positionWS, _WavesDirAngle, _WavesAmp, _WavesFreq, _WavesSpeed);
                float secondaryWave = CalculateWave(
                    vars.positionWS, 
                    _SecondaryWavesDirAngle, 
                    _SecondaryWavesAmp, 
                    _SecondaryWavesFreq, 
                    _SecondaryWavesSpeed);
                    
                float waveHeight = sin(mainWave + secondaryWave);
                float3 normalWS = TransformObjectToWorldDir(attribs.normalOS);
                vars.positionWS += waveHeight * normalWS;
                vars.positionCS = TransformWorldToHClip(vars.positionWS);

                return vars;
            }

            half4 frag(Varyings vars) : SV_Target
            {
                half3 ambient = half3(unity_SHAr.w, unity_SHAg.w, unity_SHAb.w);

                float3 normalWS = -normalize(cross(ddx(vars.positionWS), ddy(vars.positionWS)));
                float3 lightDirectionWS = normalize(_MainLightPosition.xyz);
                float lightStrength = saturate(dot(normalWS, lightDirectionWS));
                half3 diffuse = _MainLightColor * lightStrength;

                float3 reflectionDirWS = reflect(-lightDirectionWS, normalWS);
                float3 viewDirWS = normalize(_WorldSpaceCameraPos - vars.positionWS);
                half3 specular = _SpecularStrength * _MainLightColor * pow(max(dot(reflectionDirWS, viewDirWS), 0.0), _Shininess);
                return half4((ambient + diffuse + specular) * _Color.rgb, _Color.a);
            }

            ENDHLSL
        }
    }
}
