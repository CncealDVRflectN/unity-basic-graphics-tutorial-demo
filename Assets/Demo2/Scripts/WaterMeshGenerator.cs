using System.Runtime.InteropServices;
using Unity.Collections;
using UnityEngine;
using UnityEngine.Rendering;

namespace Demo2
{
    public class WaterMeshGenerator : MonoBehaviour
    {
        [SerializeField]
        private MeshFilter _meshFilter = null;

        [SerializeField, Min(1f)]
        private float _size = 10f;

        [SerializeField, Min(0)]
        private int _subdivisionsCount = 0;



#if UNITY_EDITOR
        private void OnValidate()
        {
            if (!_meshFilter) _meshFilter = GetComponent<MeshFilter>();
            if (!_meshFilter.sharedMesh) _meshFilter.sharedMesh = new Mesh { name = "Water" };

            UpdateMesh(_meshFilter.sharedMesh);
        }
#endif



        private void UpdateMesh(Mesh mesh)
        {
            Vector3 min = new Vector3(-_size / 2f, 0f, -_size / 2f);
            int sideVerticesCount = _subdivisionsCount + 2;
            int verticesCount = sideVerticesCount * sideVerticesCount;

            var vertices = new NativeArray<Attributes>(sideVerticesCount * sideVerticesCount, Allocator.Temp);
            for (int i = 0; i < sideVerticesCount; i++)
            {
                float depthParam = (float)i / (sideVerticesCount - 1);

                for (int j = 0; j < sideVerticesCount; j++)
                {
                    float widthParam = (float)j / (sideVerticesCount - 1);

                    vertices[i * sideVerticesCount + j] = new Attributes
                    {
                        Position = min + new Vector3(widthParam, 0f, depthParam) * _size,
                        Normal = Vector3.up
                    };
                }
            }

            mesh.SetVertexBufferParams(verticesCount, new[]
            {
                new VertexAttributeDescriptor(VertexAttribute.Position, VertexAttributeFormat.Float32, 3),
                new VertexAttributeDescriptor(VertexAttribute.Normal, VertexAttributeFormat.Float32, 3)
            });

            mesh.SetVertexBufferData(vertices, 0, 0, vertices.Length);
            vertices.Dispose();

            int lineTrianglesCount = 2 * (sideVerticesCount - 1);
            var indices = new NativeArray<int>(3 * lineTrianglesCount * (sideVerticesCount - 1), Allocator.Temp);
            for (int i = 0; i < sideVerticesCount - 1; i++)
            {
                for (int j = 0; j < sideVerticesCount - 1; j++)
                {
                    int dataStartIndex = 3 * i * lineTrianglesCount + 6 * j;
                    int vertexStartIndex = i * sideVerticesCount + j;

                    indices[dataStartIndex] = vertexStartIndex;
                    indices[dataStartIndex + 1] = vertexStartIndex + sideVerticesCount;
                    indices[dataStartIndex + 2] = vertexStartIndex + 1;

                    indices[dataStartIndex + 3] = indices[dataStartIndex + 2];
                    indices[dataStartIndex + 4] = indices[dataStartIndex + 1];
                    indices[dataStartIndex + 5] = indices[dataStartIndex + 1] + 1;
                }
            }

            mesh.SetIndexBufferParams(indices.Length, IndexFormat.UInt32);
            mesh.SetIndexBufferData(indices, 0, 0, indices.Length);
            mesh.subMeshCount = 1;
            mesh.SetSubMesh(
                0, 
                new SubMeshDescriptor(0, indices.Length), 
                MeshUpdateFlags.DontRecalculateBounds);
            
            indices.Dispose();
            mesh.bounds = new Bounds(Vector3.zero, new Vector3(_size, 0.01f, _size));
        }



        [StructLayout(LayoutKind.Sequential)]
        private struct Attributes
        {
            public Vector3 Position;
            public Vector3 Normal;
        }
    }
}
