using UnityEngine;

namespace Demo1
{
    [ExecuteInEditMode]
    public class LookAt : MonoBehaviour
    {
        [SerializeField]
        private Transform _target = null;


        void Update()
        {
            if (_target)
                transform.LookAt(_target);
        }
    }
}
