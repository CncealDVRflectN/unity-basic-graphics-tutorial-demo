using UnityEngine;

namespace Demo1
{
    [ExecuteInEditMode]
    public class TriangleMeshGenerator : MonoBehaviour
    {
        [SerializeField]
        private MeshFilter _meshFilter = null;



        private void Awake()
        {
            if (!_meshFilter)
                _meshFilter = GetComponent<MeshFilter>();

            Mesh mesh = new Mesh();
            mesh.vertices = new Vector3[] {
                new Vector3(-0.5f, 0.5f, 0f), 
                new Vector3(0f, -0.5f, 0f), 
                new Vector3(0.5f, 0.5f, 0f)
            };

            mesh.colors = new Color[] {
                new Color(1f, 0f, 0f, 1f),
                new Color(0f, 1f, 0f, 1f),
                new Color(0f, 0f, 1f, 1f)
            };

            mesh.triangles = new int[] { 0, 1, 2 };
            mesh.bounds = new Bounds(Vector3.zero, 1000000f * Vector3.one);

            _meshFilter.mesh = mesh;
        }
    }
}
