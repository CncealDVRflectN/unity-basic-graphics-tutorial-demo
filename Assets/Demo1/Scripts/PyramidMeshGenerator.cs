using UnityEngine;

namespace Demo1
{
    [ExecuteInEditMode]
    public class PyramidMeshGenerator : MonoBehaviour
    {
        [SerializeField]
        private MeshFilter _meshFilter = null;



        private void Awake()
        {
            if (!_meshFilter)
                _meshFilter = GetComponent<MeshFilter>();
            
            Mesh mesh = new Mesh();
            mesh.vertices = new Vector3[] {
                new Vector3(-0.5f, -0.5f, -0.5f),
                new Vector3(0f, 0.5f, 0f),
                new Vector3(0.5f, -0.5f, -0.5f),
                new Vector3(0.5f, -0.5f, -0.5f),
                new Vector3(0f, 0.5f, 0f), 
                new Vector3(0f, -0.5f, 0.5f),
                new Vector3(0f, -0.5f, 0.5f),
                new Vector3(0f, 0.5f, 0f),
                new Vector3(-0.5f, -0.5f, -0.5f),
                new Vector3(-0.5f, -0.5f, -0.5f),
                new Vector3(0.5f, -0.5f, -0.5f),
                new Vector3(0f, -0.5f, 0.5f)
            };

            mesh.colors = new Color[] {
                new Color(1f, 0f, 0f, 1f),
                new Color(1f, 0f, 0f, 1f),
                new Color(1f, 0f, 0f, 1f),
                new Color(0f, 1f, 0f, 1f),
                new Color(0f, 1f, 0f, 1f),
                new Color(0f, 1f, 0f, 1f),
                new Color(0f, 0f, 1f, 1f),
                new Color(0f, 0f, 1f, 1f),
                new Color(0f, 0f, 1f, 1f),
                new Color(1f, 1f, 1f, 1f),
                new Color(1f, 1f, 1f, 1f),
                new Color(1f, 1f, 1f, 1f)
            };

            mesh.triangles = new int[] { 
                0, 1, 2, 
                3, 4, 5,
                6, 7, 8,
                9, 10, 11
            };

            _meshFilter.mesh = mesh;
        }
    }
}
