Shader "Demo1/Basic3D"
{
    Properties
    { 
        _AlphaMultiplier("Alpha Multiplier", Range(0, 1)) = 1
    }
    SubShader
    {
        ZWrite On
        ZTest Less
        Cull Back
        Blend SrcAlpha OneMinusSrcAlpha
        BlendOp Add

        Pass
        {
            HLSLPROGRAM

            #pragma vertex vert
            #pragma fragment frag

            #include "Packages/com.unity.render-pipelines.universal/ShaderLibrary/Core.hlsl"

            struct Attributes
            {
                float3 positionOS : POSITION;
                float4 color : COLOR;
            };

            struct Varyings
            {
                float4 positionCS : SV_POSITION;
                float4 color : COLOR;
            };


            float _AlphaMultiplier;


            Varyings vert(Attributes attribs)
            {
                Varyings vars;
                vars.positionCS = TransformObjectToHClip(attribs.positionOS);
                vars.color = attribs.color;
                return vars;
            }

            half4 frag(Varyings vars) : SV_Target
            {
                half4 color = vars.color;
                color.a *= _AlphaMultiplier;
                return color;
            }

            ENDHLSL
        }
    }
}
