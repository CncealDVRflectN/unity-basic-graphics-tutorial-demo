Shader "Demo1/Basic2D_Colored"
{
    Properties
    { 
        _Color("Main Color", Color) = (1,1,1,1)
    }
    SubShader
    {
        Pass
        {
            HLSLPROGRAM
            
            #pragma vertex vert
            #pragma fragment frag

            #include "Packages/com.unity.render-pipelines.universal/ShaderLibrary/Core.hlsl"

            struct Attributes
            {
                float3 position : POSITION;
            };

            struct Varyings
            {
                float4 position : SV_POSITION;
            };


            half4 _Color;


            Varyings vert(Attributes attribs)
            {
                Varyings vars;
                vars.position = half4(attribs.position, 1.0);
                return vars;
            }

            half4 frag(Varyings vars) : SV_Target
            {
                return _Color;
            }

            ENDHLSL
        }
    }
}
