Shader "Demo1/Basic2D"
{
    SubShader
    {
        Pass
        {
            HLSLPROGRAM
            
            #pragma vertex vert
            #pragma fragment frag

            #include "Packages/com.unity.render-pipelines.universal/ShaderLibrary/Core.hlsl"

            struct Attributes
            {
                float3 position : POSITION;
                float4 color : COLOR;
            };

            struct Varyings
            {
                float4 position : SV_POSITION;
                float4 color : COLOR;
            };


            Varyings vert(Attributes attribs)
            {
                Varyings vars;
                vars.position = half4(attribs.position, 1.0);
                vars.color = attribs.color;
                return vars;
            }

            half4 frag(Varyings vars) : SV_Target
            {
                return vars.color;
            }

            ENDHLSL
        }
    }
}
